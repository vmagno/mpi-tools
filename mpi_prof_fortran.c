//   Uuseful routines for C and FORTRAN programming
//   Copyright (C) 2020  Division de Recherche en Prevision Numerique
//                       Environnement Canada
// 
//   This is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation,
//   version 2.1 of the License.
// 
//   This software is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
// Author : M.Valin, Recherche en Prevision Numerique,   2020/2021
//          V.Magnoux, Recherche en Prevision Numerique, 2020/2021
//
// Fortran specific code
// Fortran name mangling assumed : one underscore added to name
//
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

void InitMpiStats();
void DumpMpiStats();
void CloseMpiStats();
void BufSizeMpiStat(int bsize);
int FindMpiStatsEntry(const char *name);
void AddToMpiStatsEntry(int me,int bytes,int commsize,double time);

// basic macros for Fortran interface support

#define FFMPI(x,X) mpi_##x##_
#define FPMPI(x,X) pmpi_##x##_

// prototypes for ubiquitous functions in Fortran interface

void FPMPI(type_size,TYPE_SIZE)(int *datatype, int *dsize, int *ierr);
void FPMPI(comm_size,COMM_SIZE)(int *comm, int *size, int *ierr);
void FPMPI(comm_rank,COMM_RANK)(int *comm, int *rank, int *ierr);

// MPI init (Fortran and C)

void FPMPI(init,INIT)(int *ierr);
void FFMPI(init,INIT)(int *ierr)
{
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init");
  t0 = PMPI_Wtime();
  FPMPI(init,INIT)(ierr);
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(init_thread, INIT_THREAD)(int *required, int *provided, int *ierr);
void FFMPI(init_thread, INIT_THREAD)(int *required, int *provided, int *ierr)
{
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init_thread");
  t0 = PMPI_Wtime();
  FPMPI(init_thread, INIT_THREAD)(required, provided, ierr);
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

// MPI recv (Fortran and C)

void FPMPI(recv,RECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *status, int *ierr);

void FFMPI(recv,RECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *status, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Recv");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(recv,RECV)(buf,count,datatype,source,tag,comm,status,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI irecv (Fortran and C)

void FPMPI(irecv,IRECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *request, int *ierr);

void FFMPI(irecv,IRECV)(void *buf, int *count, int *datatype, int *source, int *tag,
             int *comm, int *request, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Irecv");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(irecv,IRECV)(buf,count,datatype,source,tag,comm,request,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI send (Fortran and C)

void FPMPI(send,SEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
             int *comm, int *ierr);

void FFMPI(send,SEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
             int *comm, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Send");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(send,SEND)(buf,count,datatype,dest,tag,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI isend (Fortran and C)

void FPMPI(isend,ISEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
              int *comm, int *request, int *ierr);
void FFMPI(isend,ISEND)(void *buf, int *count, int *datatype, int *dest, int *tag,
              int *comm, int *request, int *ierr)
{
  int dsize;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Isend");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(isend,ISEND)(buf,count,datatype,dest,tag,comm,request,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI reduce (Fortran and C)

void FPMPI(reduce,REDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *root, int *comm, int *ierr);
void FFMPI(reduce,REDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *root, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Reduce");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  FPMPI(comm_size,COMM_SIZE)(comm, &size, ierr);
  t0=PMPI_Wtime();
  FPMPI(reduce,REDUCE)(sendbuf,recvbuf,count,datatype,op,root,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI allreduce (Fortran and C)

void FPMPI(allreduce,ALLREDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *comm, int *ierr);
void FFMPI(allreduce,ALLREDUCE)(void *sendbuf, void *recvbuf, int *count,
            int *datatype, int *op, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Allreduce");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  FPMPI(comm_size,COMM_SIZE)(comm, &size, ierr);
  t0=PMPI_Wtime();
  FPMPI(allreduce,ALLREDUCE)(sendbuf,recvbuf,count,datatype,op,comm,ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI broadcast (Fortran and C)

void FPMPI(bcast,BCAST)(void* buffer, int *count, int *datatype, int *root, int *comm, int *ierr);
void FFMPI(bcast,BCAST)(void* buffer, int *count, int *datatype, int *root, int *comm, int *ierr)
{
  int dsize, size;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Bcast");
  FPMPI(type_size,TYPE_SIZE)(datatype, &dsize, ierr);
  FPMPI(comm_size,COMM_SIZE)(comm, &size, ierr);
  t0=PMPI_Wtime();
  FPMPI(bcast,BCAST)(buffer, count, datatype, root, comm, ierr);
  AddToMpiStatsEntry(me,(*count)*dsize,size,PMPI_Wtime()-t0);
  BufSizeMpiStat((*count)*dsize);
}
// MPI barrier

void FPMPI(barrier,BARRIER)( int *comm, int *ierr);
void FFMPI(barrier,BARRIER)( int *comm, int *ierr)
{
  double t0;
  static int me=-1;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Barrier");
  t0=PMPI_Wtime();
  FPMPI(barrier,BARRIER)(comm, ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}
// MPI testall

void FPMPI(testall,TESTALL)(int *count,int *array_of_requests,int *flag,int *array_of_statuses, int *ierr);
void FFMPI(testall,TESTALL)(int *count,int *array_of_requests,int *flag,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Testall");
  t0=PMPI_Wtime();
  FPMPI(testall,TESTALL)(count,array_of_requests,flag,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}
// MPI wait

void FPMPI(wait,WAIT)(int *array_of_requests,int *array_of_statuses, int *ierr);
void FFMPI(wait,WAIT)(int *array_of_requests,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Wait");
  t0=PMPI_Wtime();
  FPMPI(wait,WAIT)(array_of_requests,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}

// MPI waitall

void FPMPI(waitall,WAITALL)(int *count,int *array_of_requests,int *array_of_statuses, int *ierr);
void FFMPI(waitall,WAITALL)(int *count,int *array_of_requests,int *array_of_statuses, int *ierr)
{
  double t0;
  static int me=-1;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Waitall");
  t0=PMPI_Wtime();
  FPMPI(waitall,WAITALL)(count,array_of_requests,array_of_statuses,ierr);
  AddToMpiStatsEntry(me,0,1,PMPI_Wtime()-t0);
}
// MPI sendrecv

void FPMPI(sendrecv,SENDRECV)(void *sendbuf, int *sendcount, int *sendtype,
            int *dest, int *sendtag, void *recvbuf, int *recvcount,
            int *recvtype, int *source, int *recvtag,
            int *comm, int *Status, int *ierr);
void FFMPI(sendrecv,SENDRECV)(void *sendbuf, int *sendcount, int *sendtype,
            int *dest, int *sendtag, void *recvbuf, int *recvcount,
            int *recvtype, int *source, int *recvtag,
            int *comm, int *Status, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;

  if(me==-1) me=FindMpiStatsEntry("MPI_Sendrecv");
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(sendrecv,SENDRECV)(sendbuf,sendcount,sendtype,dest,sendtag,recvbuf,recvcount,recvtype,source,recvtag,comm,Status,ierr);
  AddToMpiStatsEntry(me,(*sendcount)*ssize+(*recvcount)*rsize,1,PMPI_Wtime()-t0);
  BufSizeMpiStat((*sendcount)*ssize);
  BufSizeMpiStat((*recvcount)*rsize);
}
// MPI scatter

void FPMPI(scatter,SCATTER)(void *sendbuf, int *sendcnt, int *sendtype, 
                            void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr);
void FFMPI(scatter,SCATTER)(void *sendbuf, int *sendcnt, int *sendtype, 
                            void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;

  if(me==-1) me=FindMpiStatsEntry("MPI_Scatter");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(scatter,SCATTER)(sendbuf,sendcnt,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank != *root){
    AddToMpiStatsEntry(me,(*recvcnt)*rsize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*recvcnt)*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize*size,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI scatterv

void FPMPI(scatterv,SCATTERV)(void *sendbuf, int *sendcnts, int *displs, int *sendtype, 
                              void *recvbuf, int *recvcnt, int *recvtype, int *root, int *comm, int *ierr);
void FFMPI(scatterv,SCATTERV)(void *sendbuf, int *sendcnts, int *displs, int *sendtype, 
                              void *recvbuf, int *recvcnt, int *recvtype, int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;
  int sendcnt, i;

  if(me==-1) me=FindMpiStatsEntry("MPI_Scatterv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(scatterv,SCATTERV)(sendbuf,sendcnts,displs,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank == *root) {
    sendcnt=0;
    for (i=0;i<size;i++) sendcnt+=sendcnts[i];
    AddToMpiStatsEntry(me,sendcnt*ssize,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat(sendcnts[i]*rsize);
  }else{
    AddToMpiStatsEntry(me,(*recvcnt)*rsize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*recvcnt)*rsize);
  }
}

// MPI gather

void FPMPI(gather,GATHER)(void *sendbuf, int *sendcnt, int *sendtype, 
                          void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr);
void FFMPI(gather,GATHER)(void *sendbuf, int *sendcnt, int *sendtype, 
                          void *recvbuf, int *recvcnt, int *recvtype,  int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;

  if(me==-1) me=FindMpiStatsEntry("MPI_Gather");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(gather,GATHER)(sendbuf,sendcnt,sendtype,recvbuf,recvcnt,recvtype,root,comm,ierr);
  if(rank == *root) {
    AddToMpiStatsEntry(me,(*recvcnt)*rsize*size,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat((*recvcnt)*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI gatherv

void FPMPI(gatherv,GATHERV)(void *sendbuf, int *sendcnt, int *sendtype, void *recvbuf, 
                            int *recvcnts, int *displs, int *recvtype, int *root, int *comm, int *ierr);
void FFMPI(gatherv,GATHERV)(void *sendbuf, int *sendcnt, int *sendtype, void *recvbuf, 
                            int *recvcnts, int *displs, int *recvtype, int *root, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int rank;
  int recvcnt, i;

  if(me==-1) me=FindMpiStatsEntry("MPI_Gatherv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(comm_rank,COMM_RANK)(comm,&rank,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(gatherv,GATHERV)(sendbuf,sendcnt,sendtype,recvbuf,recvcnts,displs,recvtype,root,comm,ierr);
  if(rank == *root) {
    recvcnt=0;
    for (i=0;i<size;i++) recvcnt+=recvcnts[i];
    AddToMpiStatsEntry(me,recvcnt*rsize,1,PMPI_Wtime()-t0);
    while(size--) BufSizeMpiStat(recvcnts[i]*rsize);
  }else{
    AddToMpiStatsEntry(me,(*sendcnt)*ssize,1,PMPI_Wtime()-t0);
    BufSizeMpiStat((*sendcnt)*ssize);
  }
}

// MPI alltoall

void FPMPI(alltoall,ALLTOALL)(void *sendbuf, int *sendcount, int *sendtype, 
                             void *recvbuf, int *recvcount, int *recvtype, int *comm, int *ierr);
void FFMPI(alltoall,ALLTOALL)(void *sendbuf, int *sendcount, int *sendtype, 
                             void *recvbuf, int *recvcount, int *recvtype, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Alltoall");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  t0=PMPI_Wtime();
  FPMPI(alltoall,ALLTOALL)(sendbuf,sendcount,sendtype,recvbuf,recvcount,recvtype,comm,ierr);
  AddToMpiStatsEntry(me,(*sendcount)*ssize+(*recvcount)*rsize,size,PMPI_Wtime()-t0);
  while(size--) { BufSizeMpiStat((*sendcount)*ssize); BufSizeMpiStat((*recvcount)*rsize); }
}

// MPI alltoallv

void FPMPI(alltoallv,ALLTOALLV)(void *sendbuf, int *sendcnts, int *sdispls, int *sendtype, 
                                void *recvbuf, int *recvcnts, int *rdispls, int *recvtype, int *comm, int *ierr);
void FFMPI(alltoallv,ALLTOALLV)(void *sendbuf, int *sendcnts, int *sdispls, int *sendtype, 
                                void *recvbuf, int *recvcnts, int *rdispls, int *recvtype, int *comm, int *ierr)
{
  int rsize,ssize;
  double t0;
  static int me=-1;
  int size;
  int sendcnt,recvcnt,i;
  
  if(me==-1) me=FindMpiStatsEntry("MPI_Alltoallv");
  FPMPI(comm_size,COMM_SIZE)(comm,&size,ierr);
  FPMPI(type_size,TYPE_SIZE)(sendtype, &ssize, ierr);
  FPMPI(type_size,TYPE_SIZE)(recvtype, &rsize, ierr);
  recvcnt=0;
  for (i=0;i<size;i++) { recvcnt+=recvcnts[i]; BufSizeMpiStat(recvcnts[i]*rsize) ; }
  sendcnt=0;
  for (i=0;i<size;i++) { sendcnt+=sendcnts[i]; BufSizeMpiStat(sendcnts[i]*ssize) ; }
  t0=PMPI_Wtime();
  FPMPI(alltoallv,ALLTOALLV)(sendbuf,sendcnts,sdispls,sendtype,recvbuf,recvcnts,rdispls,recvtype,comm,ierr);
  AddToMpiStatsEntry(me,sendcnt*ssize+recvcnt*rsize,1,PMPI_Wtime()-t0);
}

////////////////////////////
// One-sided communication
void FPMPI(get, GET)(void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank, MPI_Aint *target_disp,
                     int *target_count, int *target_datatype, int *window, int *ierr);
void FFMPI(get, GET)(void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank, MPI_Aint *target_disp,
                     int *target_count, int *target_datatype, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Get");

  int dsize;
  FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, ierr);
  const double t0 = PMPI_Wtime();
  FPMPI(get, GET)(origin_buf, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, window, ierr);

  AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPI(put, PUT)(const void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank,
                     MPI_Aint *target_disp, int *target_count, int *target_datatype, int *window, int *ierr);
void FFMPI(put, PUT)(const void *origin_buf, int *origin_count, int *origin_datatype, int *target_rank,
                     MPI_Aint *target_disp, int *target_count, int *target_datatype, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Put");

  int dsize;
  FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, ierr);
  const double t0 = PMPI_Wtime();
  FPMPI(put, PUT)(origin_buf, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, window, ierr);

  AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPI(accumulate, ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
                                   int *target_rank, MPI_Aint *target_disp, int *target_count,
                                   int *target_datatype, int *operation, int *window, int *ierr);
void FFMPI(accumulate, ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
                                   int *target_rank, MPI_Aint *target_disp, int *target_count,
                                   int *target_datatype, int *operation, int *window, int *ierr)
{
    static int me = -1;
    if (me == -1)
        me = FindMpiStatsEntry("MPI_Accumulate");

    int dsize;
    FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, ierr);
    const double t0 = PMPI_Wtime();
    FPMPI(accumulate, ACCUMULATE)(origin_addr, origin_count, origin_datatype, target_rank,
            target_disp, target_count, target_datatype, operation, window, ierr);
    AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPI(get_accumulate, GET_ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
        void *result_addr, int *result_count, int *result_datatype, int *target_rank, MPI_Aint *target_disp,
        int *target_count, int *target_datatype, int *operation, int *window, int *ierr);
void FFMPI(get_accumulate, GET_ACCUMULATE)(const void *origin_addr, int *origin_count, int *origin_datatype,
        void *result_addr, int *result_count, int *result_datatype, int *target_rank, MPI_Aint *target_disp,
        int *target_count, int *target_datatype, int *operation, int *window, int *ierr)
{
    static int me = -1;
    if (me == -1)
        me = FindMpiStatsEntry("MPI_Get_accumulate");

    int dsize;
    FPMPI(type_size, TYPE_SIZE)(origin_datatype, &dsize, ierr);
    const double t0 = PMPI_Wtime();
    FPMPI(get_accumulate, GET_ACCUMULATE)(
            origin_addr, origin_count, origin_datatype, result_addr, result_count, result_datatype,
            target_rank, target_disp, target_count, target_datatype, operation, window, ierr);
    AddToMpiStatsEntry(me, (*origin_count) * dsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat((*origin_count) * dsize);
}

void FPMPI(win_fence, WIN_FENCE)(int *assert, int *window, int *ierr);
void FFMPI(win_fence, WIN_FENCE)(int *assert, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_fence");

  const double t0 = PMPI_Wtime();
  FPMPI(win_fence, WIN_FENCE)(assert, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_start, WIN_START)(int *group, int *assert, int *window, int *ierr);
void FFMPI(win_start, WIN_START)(int *group, int *assert, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_start");

  const double t0 = PMPI_Wtime();
  FPMPI(win_start, WIN_START)(group, assert, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_complete, WIN_COMPLETE)(int *window, int *ierr);
void FFMPI(win_complete, WIN_COMPLETE)(int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_complete");

  const double t0 = PMPI_Wtime();
  FPMPI(win_complete, WIN_COMPLETE)(window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_post, WIN_POST)(int *group, int *assert, int *window, int *ierr);
void FFMPI(win_post, WIN_POST)(int *group, int *assert, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_post");

  const double t0 = PMPI_Wtime();
  FPMPI(win_post, WIN_POST)(group, assert, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_wait, WIN_WAIT)(int *window, int *ierr);
void FFMPI(win_wait, WIN_WAIT)(int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_wait");

  const double t0 = PMPI_Wtime();
  FPMPI(win_wait, WIN_WAIT)(window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_lock, WIN_LOCK)(int *lock_type, int *rank, int *assert, int *window, int *ierr);
void FFMPI(win_lock, WIN_LOCK)(int *lock_type, int *rank, int *assert, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock");

  const double t0 = PMPI_Wtime();
  FPMPI(win_lock, WIN_LOCK)(lock_type, rank, assert, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_lock_all, WIN_LOCK_ALL)(int *assert, int *window, int *ierr);
void FFMPI(win_lock_all, WIN_LOCK_ALL)(int *assert, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_lock_all, WIN_LOCK_ALL)(assert, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_unlock, WIN_UNLOCK)(int *rank, int *window, int *ierr);
void FFMPI(win_unlock, WIN_UNLOCK)(int *rank, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock");

  const double t0 = PMPI_Wtime();
  FPMPI(win_unlock, WIN_UNLOCK)(rank, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_unlock_all, WIN_UNLOCK_ALL)(int *window, int *ierr);
void FFMPI(win_unlock_all, WIN_UNLOCK_ALL)(int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_unlock_all, WIN_UNLOCK_ALL)(window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush, WIN_FLUSH)(int *rank, int *window, int *ierr);
void FFMPI(win_flush, WIN_FLUSH)(int *rank, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush, WIN_FLUSH)(rank, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_local, WIN_FLUSH_LOCAL)(int *rank, int *window, int *ierr);
void FFMPI(win_flush_local, WIN_FLUSH_LOCAL)(int *rank, int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_local, WIN_FLUSH_LOCAL)(rank, window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_all, WIN_FLUSH_ALL)(int *window, int *ierr);
void FFMPI(win_flush_all, WIN_FLUSH_ALL)(int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_all, WIN_FLUSH_ALL)(window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

void FPMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(int *window, int *ierr);
void FFMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(int *window, int *ierr)
{
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local_all");

  const double t0 = PMPI_Wtime();
  FPMPI(win_flush_local_all, WIN_FLUSH_LOCAL_ALL)(window, ierr);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
}

// MPI finalize (Fortran and C)

void FFMPI(finalize,FINALIZE)(int *ierr)
{
  DumpMpiStats();
  CloseMpiStats();
  *ierr = PMPI_Finalize();
}

