//   Useful routines for C and FORTRAN programming
//   Copyright (C) 2020  Division de Recherche en Prevision Numerique
//                       Environnement Canada
//
//   This is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation,
//   version 2.1 of the License.
//
//   This software is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
// Author : M.Valin,   Recherche en Prevision Numerique, Oct 2020
//          V.Magnoux, Recherche en Prevision Numerique, Oct 2020
//
// C specific code
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void InitMpiStats();
void DumpMpiStats();
void CloseMpiStats();
void BufSizeMpiStat(int bsize);
int  FindMpiStatsEntry(const char* name);
void AddToMpiStatsEntry(int me, int bytes, int commsize, double time);

// MPI init (C)

int MPI_Init(int* argc, char*** argv) {
  int rc;
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init");
  t0 = PMPI_Wtime();
  rc = PMPI_Init(argc, argv);
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return (rc);
}

int MPI_Init_thread(int* argc, char*** argv, int required, int* provided) {
  int status ;
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Init_thread");
  t0 = PMPI_Wtime();
  status = PMPI_Init_thread(argc, argv, required, provided);
  InitMpiStats();
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return status;
}

// MPI recv (C)

int MPI_Recv(void* buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status* Status) {
  int        dsize;
  int        status;
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Recv");
  PMPI_Type_size(datatype, &dsize);
  t0     = PMPI_Wtime();
  status = PMPI_Recv(buf, count, datatype, source, tag, comm, Status);
  AddToMpiStatsEntry(me, count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI irecv (C)

int MPI_Irecv(void* buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request* request) {
  int        dsize;
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Irecv");
  PMPI_Type_size(datatype, &dsize);
  t0     = PMPI_Wtime();
  status = PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
  AddToMpiStatsEntry(me, count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI send (C)

int MPI_Send(const void* buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm) {
  int        dsize;
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Send");
  PMPI_Type_size(datatype, &dsize);
  t0     = PMPI_Wtime();
  status = PMPI_Send(buf, count, datatype, dest, tag, comm);
  AddToMpiStatsEntry(me, count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI isend (C)

int MPI_Isend(
    const void* buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request* request) {
  int        dsize;
  int        status;
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Isend");
  PMPI_Type_size(datatype, &dsize);
  t0     = PMPI_Wtime();
  status = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
  AddToMpiStatsEntry(me, count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI reduce (C)

int MPI_Reduce(
    const void* sendbuf, void* recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm) {
  int        dsize, size;
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Reduce");
  PMPI_Type_size(datatype, &dsize);
  PMPI_Comm_size(comm, &size);
  t0     = PMPI_Wtime();
  status = PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
  AddToMpiStatsEntry(me, count * dsize, size, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI allreduce (C)

int MPI_Allreduce(const void* sendbuf, void* recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm) {
  int        dsize, size;
  int        status;
  double     t0;
  static int me = -1;
  //   double *s=(double *)sendbuf;
  //   double *r=(double *)recvbuf;

  //   if( op==MPI_MAX && datatype==MPI_DOUBLE && r+2==s  &&
  //       count==2 && s[0]==654321.0 && s[1]==123456.0  ){
  //     r[0]=pmpi_r_statistics.time;
  //     r[1]=pmpi_r_statistics.calls;
  //     return(MPI_SUCCESS);
  //   }
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Allreduce");
  PMPI_Type_size(datatype, &dsize);
  PMPI_Comm_size(comm, &size);
  t0     = PMPI_Wtime();
  status = PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
  AddToMpiStatsEntry(me, count * dsize, size, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI broadcast (C)

int MPI_Bcast(void* buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm) {
  int        dsize, size;
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Bcast");
  PMPI_Type_size(datatype, &dsize);
  PMPI_Comm_size(comm, &size);
  t0     = PMPI_Wtime();
  status = PMPI_Bcast(buffer, count, datatype, root, comm);
  AddToMpiStatsEntry(me, count * dsize, size, PMPI_Wtime() - t0);
  BufSizeMpiStat(count * dsize);
  return status;
}

// MPI barrier

int MPI_Barrier(MPI_Comm comm) {
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Barrier");
  t0     = PMPI_Wtime();
  status = PMPI_Barrier(comm);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return status;
}

// MPI testall

int MPI_Testall(int count, MPI_Request* array_of_requests, int* flag, MPI_Status* array_of_statuses) {
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Testall");
  t0     = PMPI_Wtime();
  status = PMPI_Testall(count, array_of_requests, flag, array_of_statuses);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return status;
}

// MPI wait

int MPI_Wait(MPI_Request* array_of_requests, MPI_Status* array_of_statuses) {
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Wait");
  t0     = PMPI_Wtime();
  status = PMPI_Wait(array_of_requests, array_of_statuses);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return status;
}

// MPI waitall

int MPI_Waitall(int count, MPI_Request* array_of_requests, MPI_Status* array_of_statuses) {
  int        status;
  double     t0;
  static int me = -1;

  if (me == -1)
    me = FindMpiStatsEntry("MPI_Waitall");
  t0     = PMPI_Wtime();
  status = PMPI_Waitall(count, array_of_requests, array_of_statuses);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);
  return status;
}

// MPI sendrecv

int MPI_Sendrecv(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    int          dest,
    int          sendtag,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    int          source,
    int          recvtag,
    MPI_Comm     comm,
    MPI_Status*  Status) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Sendrecv");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Sendrecv(
      sendbuf, sendcount, sendtype, dest, sendtag, recvbuf, recvcount, recvtype, source, recvtag, comm, Status);
  AddToMpiStatsEntry(me, sendcount * ssize + recvcount * rsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(sendcount * ssize);
  BufSizeMpiStat(recvcount * rsize);
  return status;
}

// MPI scatter

int MPI_Scatter(
    const void*  sendbuf,
    int          sendcnt,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcnt,
    MPI_Datatype recvtype,
    int          root,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;
  int        rank;

  PMPI_Comm_size(comm, &size);
  PMPI_Comm_rank(comm, &rank);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Scatter");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Scatter(sendbuf, sendcnt, sendtype, recvbuf, recvcnt, recvtype, root, comm);
  if (rank != root) {
    AddToMpiStatsEntry(me, recvcnt * rsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat(recvcnt * rsize);
  }
  else {
    AddToMpiStatsEntry(me, sendcnt * ssize * size, 1, PMPI_Wtime() - t0);
    while (size--)
      BufSizeMpiStat(sendcnt * ssize);
  }
  return status;
}

// MPI scatterv

int MPI_Scatterv(
    const void*  sendbuf,
    const int*   sendcnts,
    const int*   displs,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcnt,
    MPI_Datatype recvtype,
    int          root,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;
  int        rank;
  int        sendcnt, i;

  PMPI_Comm_size(comm, &size);
  PMPI_Comm_rank(comm, &rank);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Scatterv");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Scatterv(sendbuf, sendcnts, displs, sendtype, recvbuf, recvcnt, recvtype, root, comm);
  if (rank == root) {
    sendcnt = 0;
    for (i = 0; i < size; i++)
      sendcnt += sendcnts[i];
    AddToMpiStatsEntry(me, sendcnt * ssize, 1, PMPI_Wtime() - t0);
    while (size--)
      BufSizeMpiStat(sendcnts[i] * rsize);
  }
  else {
    AddToMpiStatsEntry(me, recvcnt * rsize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat(recvcnt * rsize);
  }
  return status;
}

// MPI gather

int MPI_Gather(
    const void*  sendbuf,
    int          sendcnt,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcnt,
    MPI_Datatype recvtype,
    int          root,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;
  int        rank;

  PMPI_Comm_size(comm, &size);
  PMPI_Comm_rank(comm, &rank);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Gather");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Gather(sendbuf, sendcnt, sendtype, recvbuf, recvcnt, recvtype, root, comm);
  if (rank == root) {
    AddToMpiStatsEntry(me, recvcnt * rsize * size, 1, PMPI_Wtime() - t0);
    while (size--)
      BufSizeMpiStat(recvcnt * rsize);
  }
  else {
    AddToMpiStatsEntry(me, sendcnt * ssize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat(sendcnt * ssize);
  }
  return status;
}

// MPI gatherv

int MPI_Gatherv(
    const void*  sendbuf,
    int          sendcnt,
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int*   recvcnts,
    const int*   displs,
    MPI_Datatype recvtype,
    int          root,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;
  int        rank;
  int        recvcnt, i;

  PMPI_Comm_size(comm, &size);
  PMPI_Comm_rank(comm, &rank);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Gatherv");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Gatherv(sendbuf, sendcnt, sendtype, recvbuf, recvcnts, displs, recvtype, root, comm);
  if (rank == root) {
    recvcnt = 0;
    for (i = 0; i < size; i++)
      recvcnt += recvcnts[i];
    AddToMpiStatsEntry(me, recvcnt * rsize, 1, PMPI_Wtime() - t0);
    while (size--)
      BufSizeMpiStat(recvcnts[i] * rsize);
  }
  else {
    AddToMpiStatsEntry(me, sendcnt * ssize, 1, PMPI_Wtime() - t0);
    BufSizeMpiStat(sendcnt * ssize);
  }
  return status;
}

// MPI alltoall

int MPI_Alltoall(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;

  PMPI_Comm_size(comm, &size);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Alltoall");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  t0     = PMPI_Wtime();
  status = PMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
  AddToMpiStatsEntry(me, sendcount * ssize + recvcount * rsize, size, PMPI_Wtime() - t0);
  while (size--) {
    BufSizeMpiStat(sendcount * ssize);
    BufSizeMpiStat(recvcount * rsize);
  }
  return status;
}

// MPI alltoallv

int MPI_Alltoallv(
    const void*  sendbuf,
    const int*   sendcnts,
    const int*   sdispls,
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int*   recvcnts,
    const int*   rdispls,
    MPI_Datatype recvtype,
    MPI_Comm     comm) {
  int        rsize, ssize;
  int        status;
  double     t0;
  static int me = -1;
  int        size;
  int        sendcnt, recvcnt, i;

  PMPI_Comm_size(comm, &size);
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Alltoallv");
  PMPI_Type_size(sendtype, &ssize);
  PMPI_Type_size(recvtype, &rsize);
  recvcnt = 0;
  for (i = 0; i < size; i++) {
    recvcnt += recvcnts[i];
    BufSizeMpiStat(recvcnts[i] * rsize);
  }
  sendcnt = 0;
  for (i = 0; i < size; i++) {
    sendcnt += sendcnts[i];
    BufSizeMpiStat(sendcnts[i] * ssize);
  }
  t0     = PMPI_Wtime();
  status = PMPI_Alltoallv(sendbuf, sendcnts, sdispls, sendtype, recvbuf, recvcnts, rdispls, recvtype, comm);
  AddToMpiStatsEntry(me, sendcnt * ssize + recvcnt * rsize, size, PMPI_Wtime() - t0);
  return status;
}

/////////////////////////
// Neighbor collectives

int MPI_Neighbor_allgather(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    MPI_Comm     comm //
) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Neighbor_allgather");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Neighbor_allgather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recvcount * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_in_neighbors--)
    BufSizeMpiStat(recvcount * recv_data_size);
  while (num_out_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);

  return status;
}

int MPI_Neighbor_allgatherv(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int    recvcounts[],
    const int    displs[],
    MPI_Datatype recvtype,
    MPI_Comm     comm //
) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Neighbor_allgatherv");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    recv_count += recvcounts[i];
    BufSizeMpiStat(recvcounts[i] * recv_data_size);
  }

  const double t0 = PMPI_Wtime();
  const int    status =
      PMPI_Neighbor_allgatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, comm);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recv_count * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_out_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);

  return status;
}

int MPI_Neighbor_alltoall(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    MPI_Comm     comm) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Neighbor_alltoall");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Neighbor_alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recvcount * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_in_neighbors--)
    BufSizeMpiStat(recvcount * recv_data_size);
  while (num_out_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);

  return status;
}

int MPI_Neighbor_alltoallv(
    const void*  sendbuf,
    const int    sendcounts[],
    const int    sdispls[],
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int    recvcounts[],
    const int    rdispls[],
    MPI_Datatype recvtype,
    MPI_Comm     comm) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Neighbor_alltoallv");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_size, recv_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_size);
  PMPI_Type_size(recvtype, &recv_size);

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    recv_count += recvcounts[i];
    BufSizeMpiStat(recvcounts[i] * recv_size);
  }
  int send_count = 0;
  for (int i = 0; i < num_out_neighbors; i++) {
    send_count += sendcounts[i];
    BufSizeMpiStat(sendcounts[i] * send_size);
  }

  const double t0 = PMPI_Wtime();
  const int    status =
      PMPI_Neighbor_alltoallv(sendbuf, sendcounts, sdispls, sendtype, recvbuf, recvcounts, rdispls, recvtype, comm);

  AddToMpiStatsEntry(me, send_count * send_size + recv_count * recv_size, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Neighbor_alltoallw(
    const void*        sendbuf,
    const int          sendcounts[],
    const MPI_Aint     sdispls[],
    const MPI_Datatype sendtypes[],
    void*              recvbuf,
    const int          recvcounts[],
    const MPI_Aint     rdispls[],
    const MPI_Datatype recvtypes[],
    MPI_Comm           comm) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Neighbor_alltoallw");

  int comm_size, num_in_neighbors, num_out_neighbors, weighted, data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);

  int total_num_bytes = 0;

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    PMPI_Type_size(recvtypes[i], &data_size);
    recv_count += recvcounts[i];
    const int num_bytes = recvcounts[i] * data_size;
    BufSizeMpiStat(num_bytes);
    total_num_bytes += num_bytes;
  }

  int send_count = 0;
  for (int i = 0; i < num_out_neighbors; i++) {
    PMPI_Type_size(sendtypes[i], &data_size);
    send_count += sendcounts[i];
    const int num_bytes = sendcounts[i] * data_size;
    BufSizeMpiStat(num_bytes);
    total_num_bytes += num_bytes;
  }

  const double t0 = PMPI_Wtime();
  const int    status =
      PMPI_Neighbor_alltoallw(sendbuf, sendcounts, sdispls, sendtypes, recvbuf, recvcounts, rdispls, recvtypes, comm);

  AddToMpiStatsEntry(me, total_num_bytes, 1, PMPI_Wtime() - t0);

  return status;
}

// Non blocking neighbor collectives

int MPI_Ineighbor_allgather(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    MPI_Comm     comm,
    MPI_Request* request //
) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Ineighbor_allgather");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  const double t0 = PMPI_Wtime();
  const int    status =
      PMPI_Ineighbor_allgather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, request);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recvcount * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_in_neighbors--)
    BufSizeMpiStat(recvcount * recv_data_size);
  while (num_out_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);

  return status;
}

int MPI_Ineighbor_allgatherv(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int    recvcounts[],
    const int    displs[],
    MPI_Datatype recvtype,
    MPI_Comm     comm,
    MPI_Request* request //
) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Ineighbor_allgatherv");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    recv_count += recvcounts[i];
    BufSizeMpiStat(recvcounts[i] * recv_data_size);
  }

  const double t0 = PMPI_Wtime();
  const int    status =
      PMPI_Ineighbor_allgatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, comm, request);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recv_count * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_out_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);

  return status;
}

int MPI_Ineighbor_alltoall(
    const void*  sendbuf,
    int          sendcount,
    MPI_Datatype sendtype,
    void*        recvbuf,
    int          recvcount,
    MPI_Datatype recvtype,
    MPI_Comm     comm,
    MPI_Request* request) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Ineighbor_alltoall");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_data_size, recv_data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_data_size);
  PMPI_Type_size(recvtype, &recv_data_size);

  const double t0  = PMPI_Wtime();
  const int status = PMPI_Ineighbor_alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, request);
  AddToMpiStatsEntry(me, sendcount * send_data_size + recvcount * recv_data_size, 1, PMPI_Wtime() - t0);

  while (num_in_neighbors--)
    BufSizeMpiStat(sendcount * send_data_size);
  while (num_out_neighbors--)
    BufSizeMpiStat(recvcount * recv_data_size);

  return status;
}

int MPI_Ineighbor_alltoallv(
    const void*  sendbuf,
    const int    sendcounts[],
    const int    sdispls[],
    MPI_Datatype sendtype,
    void*        recvbuf,
    const int    recvcounts[],
    const int    rdispls[],
    MPI_Datatype recvtype,
    MPI_Comm     comm,
    MPI_Request* request) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Ineighbor_alltoallv");

  int comm_size;
  int num_in_neighbors, num_out_neighbors, weighted;
  int send_size, recv_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);
  PMPI_Type_size(sendtype, &send_size);
  PMPI_Type_size(recvtype, &recv_size);

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    recv_count += recvcounts[i];
    BufSizeMpiStat(recvcounts[i] * recv_size);
  }
  int send_count = 0;
  for (int i = 0; i < num_out_neighbors; i++) {
    send_count += sendcounts[i];
    BufSizeMpiStat(sendcounts[i] * send_size);
  }

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Ineighbor_alltoallv(
      sendbuf, sendcounts, sdispls, sendtype, recvbuf, recvcounts, rdispls, recvtype, comm, request);

  AddToMpiStatsEntry(me, send_count * send_size + recv_count * recv_size, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Ineighbor_alltoallw(
    const void*        sendbuf,
    const int          sendcounts[],
    const MPI_Aint     sdispls[],
    const MPI_Datatype sendtypes[],
    void*              recvbuf,
    const int          recvcounts[],
    const MPI_Aint     rdispls[],
    const MPI_Datatype recvtypes[],
    MPI_Comm           comm,
    MPI_Request*       request) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Ineighbor_alltoallw");

  int comm_size, num_in_neighbors, num_out_neighbors, weighted, data_size;
  PMPI_Comm_size(comm, &comm_size);
  PMPI_Dist_graph_neighbors_count(comm, &num_in_neighbors, &num_out_neighbors, &weighted);

  int total_num_bytes = 0;

  int recv_count = 0;
  for (int i = 0; i < num_in_neighbors; i++) {
    PMPI_Type_size(recvtypes[i], &data_size);
    recv_count += recvcounts[i];
    const int num_bytes = recvcounts[i] * data_size;
    BufSizeMpiStat(num_bytes);
    total_num_bytes += num_bytes;
  }

  int send_count = 0;
  for (int i = 0; i < num_out_neighbors; i++) {
    PMPI_Type_size(sendtypes[i], &data_size);
    send_count += sendcounts[i];
    const int num_bytes = sendcounts[i] * data_size;
    BufSizeMpiStat(num_bytes);
    total_num_bytes += num_bytes;
  }

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Ineighbor_alltoallw(
      sendbuf, sendcounts, sdispls, sendtypes, recvbuf, recvcounts, rdispls, recvtypes, comm, request);

  AddToMpiStatsEntry(me, total_num_bytes, 1, PMPI_Wtime() - t0);

  return status;
}

////////////////////////////////////
// 1-sided communication functions

int MPI_Get(
    void*        origin_addr,
    int          origin_count,
    MPI_Datatype origin_datatype,
    int          target_rank,
    MPI_Aint     target_disp,
    int          target_count,
    MPI_Datatype target_datatype,
    MPI_Win      win) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Get");

  int dsize;
  PMPI_Type_size(origin_datatype, &dsize);
  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Get(
      origin_addr, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, win);

  AddToMpiStatsEntry(me, origin_count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(origin_count * dsize);

  return status;
}

int MPI_Put(
    const void*  origin_addr,
    int          origin_count,
    MPI_Datatype origin_datatype,
    int          target_rank,
    MPI_Aint     target_disp,
    int          target_count,
    MPI_Datatype target_datatype,
    MPI_Win      window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Put");

  int dsize;
  PMPI_Type_size(origin_datatype, &dsize);
  const double t0 = PMPI_Wtime();

  const int status = PMPI_Put(
      origin_addr, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, window);

  AddToMpiStatsEntry(me, origin_count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(origin_count * dsize);

  return status;
}

int MPI_Accumulate(
    const void*  origin_addr,
    int          origin_count,
    MPI_Datatype origin_datatype,
    int          target_rank,
    MPI_Aint     target_disp,
    int          target_count,
    MPI_Datatype target_datatype,
    MPI_Op       operation,
    MPI_Win      window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Accumulate");

  int dsize;
  PMPI_Type_size(origin_datatype, &dsize);
  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Accumulate(
      origin_addr, origin_count, origin_datatype, target_rank, target_disp, target_count, target_datatype, operation,
      window);
  AddToMpiStatsEntry(me, origin_count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(origin_count * dsize);

  return status;
}

int MPI_Get_accumulate(
    const void*  origin_addr,
    int          origin_count,
    MPI_Datatype origin_datatype,
    void*        result_addr,
    int          result_count,
    MPI_Datatype result_datatype,
    int          target_rank,
    MPI_Aint     target_disp,
    int          target_count,
    MPI_Datatype target_datatype,
    MPI_Op       operation,
    MPI_Win      window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Get_accumulate");

  int dsize;
  PMPI_Type_size(origin_datatype, &dsize);
  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Get_accumulate(
      origin_addr, origin_count, origin_datatype, result_addr, result_count, result_datatype, target_rank, target_disp,
      target_count, target_datatype, operation, window);
  AddToMpiStatsEntry(me, origin_count * dsize, 1, PMPI_Wtime() - t0);
  BufSizeMpiStat(origin_count * dsize);

  return status;
}

int MPI_Win_fence(int assert, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_fence");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_fence(assert, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_start(MPI_Group group, int assert, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_start");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_start(group, assert, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_complete(MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_complete");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_complete(window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_post(MPI_Group group, int assert, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_post");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_post(group, assert, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_wait(MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_wait");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_wait(window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_lock(int lock_type, int rank, int assert, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_lock(lock_type, rank, assert, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_lock_all(int assert, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_lock_all");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_lock_all(assert, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_unlock(int rank, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_unlock(rank, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_unlock_all(MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_unlock_all");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_unlock_all(window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_flush(int rank, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_flush(rank, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_flush_local(int rank, MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_flush_local(rank, window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_flush_all(MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_all");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_flush_all(window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_flush_local_all(MPI_Win window) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_flush_local_all");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_flush_local_all(window);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

int MPI_Win_sync(MPI_Win win) {
  static int me = -1;
  if (me == -1)
    me = FindMpiStatsEntry("MPI_Win_sync");

  const double t0     = PMPI_Wtime();
  const int    status = PMPI_Win_sync(win);
  AddToMpiStatsEntry(me, 0, 1, PMPI_Wtime() - t0);

  return status;
}

// MPI finalize (C)

int MPI_Finalize(void) {
  DumpMpiStats();
  CloseMpiStats();
  return (PMPI_Finalize());
}
