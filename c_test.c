#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// quick and dirty MPI C test for the MPI profiler

int main(int argc, char **argv)
{
  int my_rank = -1;                                                                                                     
  int my_size = -1;                                                                                                     
  char hostname[1204];
  char *MP_CHILD = getenv("MP_CHILD");
  int buf[10];
  int buf2[10];
  MPI_Status status;

  if (MP_CHILD == NULL) MP_CHILD = "-1";
  gethostname(hostname, 1023);
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &my_size);
  if (my_size != 2) goto the_end;
  MPI_Bcast(&buf, 9, MPI_INTEGER, 0, MPI_COMM_WORLD);
  MPI_Reduce(&buf, &status, 5, MPI_INTEGER, MPI_BOR, 0, MPI_COMM_WORLD);
  MPI_Allreduce(&buf, &buf2, 7, MPI_INTEGER, MPI_BOR, MPI_COMM_WORLD);
  MPI_Alltoall(&buf, 3, MPI_INTEGER, &buf2, 3, MPI_INTEGER, MPI_COMM_WORLD);
  MPI_Alltoall(&buf, 5, MPI_INTEGER, &buf2, 5, MPI_INTEGER, MPI_COMM_WORLD);
  if (my_rank == 0) {
    MPI_Send(&buf, 10, MPI_INTEGER, 1, 0, MPI_COMM_WORLD);
    MPI_Send(&buf, 8, MPI_INTEGER, 1, 0, MPI_COMM_WORLD);
    MPI_Recv(&buf, 6, MPI_INTEGER, 1, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(&buf, 4, MPI_INTEGER, 1, 0, MPI_COMM_WORLD, &status);
    MPI_Sendrecv(&buf, 9, MPI_INTEGER, 1, 0, &buf2, 9, MPI_INTEGER, 1, 0, MPI_COMM_WORLD, &status);
    MPI_Sendrecv(&buf, 7, MPI_INTEGER, 1, 0, &buf2, 7, MPI_INTEGER, 1, 0, MPI_COMM_WORLD, &status);
  } else {
    MPI_Recv(&buf, 10, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(&buf, 8, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Send(&buf, 6, MPI_INTEGER, 0, 0, MPI_COMM_WORLD);
    MPI_Send(&buf, 4, MPI_INTEGER, 0, 0, MPI_COMM_WORLD);
    MPI_Sendrecv(&buf, 9, MPI_INTEGER, 0, 0,& buf2, 9, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Sendrecv(&buf, 7, MPI_INTEGER, 0, 0, &buf2, 7, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, &status);
  }
  MPI_Bcast(&buf, 7, MPI_INTEGER, 1, MPI_COMM_WORLD);
  MPI_Reduce(&buf, &status, 7, MPI_INTEGER, MPI_BOR, 0, MPI_COMM_WORLD);
  MPI_Allreduce(&buf, &buf2, 9, MPI_INTEGER, MPI_BOR, MPI_COMM_WORLD);


  // Non blocking send/receive
  int target = (my_rank + 1) % my_size;
  int source = (my_rank + my_size - 1) % my_size;

  MPI_Request requests[2];
  MPI_Status  statuses[2];

  MPI_Isend(buf, 10, MPI_INTEGER, target, 0, MPI_COMM_WORLD, &requests[0]);
  MPI_Irecv(buf2, 10, MPI_INTEGER, source, 0, MPI_COMM_WORLD, &requests[1]);

  MPI_Waitall(2, requests, statuses);

  //////////////////////////
  // 1-sided communication
  int shared_buffer[10];
  int local_buffer[10];

  MPI_Win window;
  MPI_Win_create(shared_buffer, 10 * sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &window);

  // Fence synchronization
  for (int i = 0; i < 10; i++)
  {
    shared_buffer[i] = my_rank * 10 + i;
    local_buffer[i] = 0;
  }

  MPI_Win_fence(0, window);

  target = (my_rank + 1) % my_size;
  MPI_Get(local_buffer, 10, MPI_INTEGER, target, 0, 10, MPI_INTEGER, window); 

  MPI_Win_fence(0, window);

  target = (my_rank + my_size - 1) % my_size;
  MPI_Put(local_buffer, 10, MPI_INTEGER, target, 0, 10, MPI_INTEGER, window);

  MPI_Win_fence(0, window);

  // Post/start complete/wait synchronization

  MPI_Group world_group, small_group;
  int ranks[1];

  MPI_Comm_group(MPI_COMM_WORLD, &world_group);

  for (int i = 0; i < 10; i++)
    local_buffer[i] = -1;

  MPI_Win_fence(0, window); // Must sync because we wrote (locally) to the shared buffer

  if (my_rank == 0)
  {
    ranks[0] = 1;
    MPI_Group_incl(world_group, 1, ranks, &small_group);
    MPI_Win_start(small_group, 0, window); // Blocking (maybe)

    MPI_Get(local_buffer, 5, MPI_INTEGER, 1, 0, 5, MPI_INTEGER, window);
    MPI_Put(local_buffer + 5, 5, MPI_INTEGER, 1, 5, 5, MPI_INTEGER, window);

    MPI_Win_complete(window);
  }
  else if (my_rank == 1)
  {
    ranks[0] = 0;
    MPI_Group_incl(world_group, 1, ranks, &small_group);
    MPI_Win_post(small_group, 0, window);
    MPI_Win_wait(window);
  }

  for (int i = 0; i < 10; i++)
  {
    printf("%02d ", local_buffer[i]);
  }
  printf("\n");

  MPI_Barrier(MPI_COMM_WORLD);


  // Passive synchronization

  for (int i = 0; i < 10; i++)
  {
    local_buffer[i] = -1;
    shared_buffer[i] = 10 * my_rank + i;
  }

  MPI_Win_fence(0, window); // Don't forget to sync after writing

  if (my_rank == 0)
  {
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 1, 0, window);
    
    MPI_Get(local_buffer, 5, MPI_INTEGER, 1, 0, 5, MPI_INTEGER, window);
    MPI_Win_flush_local(1, window); // Can do stuff with local buffer after this

    MPI_Put(local_buffer + 5, 5, MPI_INTEGER, 1, 5, 5, MPI_INTEGER, window);
    MPI_Win_flush(1, window); // Not needed since we call unlock right after (just want to test)

    MPI_Win_unlock(1, window);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  for (int i = 0; i < 10; i++)
  {
    printf("%02d ", local_buffer[i]);
  }
  printf("\n");

  // Accumulate functions
  if (my_rank == 0)
  {
      for (int i = 0; i < 10; i++)
          shared_buffer[i] = i;
  }
  else if (my_rank == 1)
  {
      for (int i = 0; i < 10 ; i++)
          shared_buffer[i] = 10 - i;
  }

  MPI_Win_fence(0, window);
  if (my_rank == 0)
      MPI_Accumulate(shared_buffer, 10, MPI_INTEGER, 1, 0, 10, MPI_INTEGER, MPI_SUM, window);

  MPI_Win_fence(0, window);

  if (my_rank == 0)
  {
      MPI_Get_accumulate(shared_buffer, 10, MPI_INTEGER,
                         shared_buffer, 10, MPI_INTEGER,
                         1, 0, 10, MPI_INTEGER, MPI_PROD, window);
  }

  MPI_Win_fence(0, window);

  for (int i = 0; i < 10; i++)
  {
    printf("%02d ", shared_buffer[i]);
  }
  printf("\n");


  // Check that the other instrumented functions are ok

  if (my_rank == 0)
  {
    MPI_Win_lock_all(0, window);
    MPI_Win_flush_all(window);
    MPI_Win_flush_local_all(window);
    MPI_Win_unlock_all(window);
  }

  MPI_Win_free(&window);


the_end:
  printf("host = %s, rank = %d, MP_CHILD=%s\n",hostname,my_rank,MP_CHILD);
  MPI_Finalize();
}
